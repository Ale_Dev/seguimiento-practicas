import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { Responsable } from './models/responsable.model';

@Injectable()
export class ResponsableService {

  public API = '//localhost:8080';
  public RESPONSABLE_API = this.API + '/responsables';

  constructor(private http: HttpClient) { }

  get(id: string){
    return this.http.get(this.RESPONSABLE_API + '/'+id);
  }

  //llama al servicio de la API REST que consulta todos los ciclos de la BBDD
  getAll(): Observable<any> {
    return this.http.get<Responsable[]>(this.RESPONSABLE_API + '/all');
  }

  getByEmpresa(idEmpresa){
    return this.http.get<Responsable[]>(this.RESPONSABLE_API + '/empresa/' + idEmpresa);
  }

  createResponsable(responsable){
    return this.http.post<Responsable>(this.RESPONSABLE_API + '/add', responsable);
  }

  deleteResponsable(responsable: Responsable){
    return this.http.delete(this.RESPONSABLE_API + '/delete/' + responsable.idResponsable);
  }

}
