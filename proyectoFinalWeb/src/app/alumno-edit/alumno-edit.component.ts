import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';

//Servicios
import { AlumnoService } from '../alumno.service';
import { CicloService } from '../ciclo.service';
import { EmpresaService } from '../empresa.service';
import { ResponsableService } from '../responsable.service';

//Clases
import { Alumno } from '../models/alumno.model';
import { Ciclo } from '../models/ciclo.model';
import { Empresa } from '../models/empresa.model';
import { Responsable } from '../models/responsable.model';

@Component({
  selector: 'app-alumno-edit',
  templateUrl: './alumno-edit.component.html',
  styleUrls: ['./alumno-edit.component.css']
})
export class AlumnoEditComponent implements OnInit {

  alumno: Alumno = {
    "idAlumno": "",
    "nombre": "",
    "apellidos": "",
    "email": "",
    "pass": "",
    "idResponsableAux": "",
    "idCicloAux": ""
  };

  sub: Subscription;

  ciclos: Array<Ciclo>;
  empresas: Array<Empresa>;
  responsables: Array<Responsable>;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private alumnoService: AlumnoService,
    private cicloService: CicloService,
    private empresaService: EmpresaService,
    private responsableService: ResponsableService) { }

  ngOnInit() {
    this.cicloService.getAll().subscribe(data => {
      this.ciclos = data;
    });

    this.empresaService.getAll().subscribe(data => {
      this.empresas = data;
    });

    this.sub = this.route.params.subscribe(params => {
      const id = params['id'];

      if(id){
        this.alumnoService.get(id).subscribe((alumno: any) => {
          if(alumno){
            this.alumno = alumno;
          }
          else{
            console.log('El alumno no existe');
            this.gotoList();
          }
        });
      }
    });
  }

  mostrarResponsables(idEmpresa: string){
    let responsable = document.getElementById("Responsable");

    this.responsableService.getByEmpresa(idEmpresa).subscribe(data => {
      this.responsables = data;
    });

    responsable.removeAttribute("disabled");
  }

  gotoList(){
    this.router.navigate(['/lista-alumnos']);
  }

  validarmail(mail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
        return (true);
    }else{
      return (false);
    } 
}

  createAlumno(): void{
    if(this.alumno.nombre == "" || this.alumno.apellidos == "" || this.alumno.email == "" ||
     this.alumno.pass == "" || this.alumno.idResponsableAux == "" || this.alumno.idCicloAux == ""
     || !this.validarmail(this.alumno.email)){
      alert("Rellena todo el formulario");
     }else{
      this.alumnoService.createAlumno(this.alumno).subscribe( result => {
        this.gotoList();
      });
     }
  }
}
