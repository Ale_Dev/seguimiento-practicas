import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';

import { ResponsableService } from '../responsable.service';
import { EmpresaService } from '../empresa.service';

import { Responsable } from '../models/responsable.model';
import { Empresa } from '../models/empresa.model';

@Component({
  selector: 'app-responsable-edit',
  templateUrl: './responsable-edit.component.html',
  styleUrls: ['./responsable-edit.component.css']
})
export class ResponsableEditComponent implements OnInit {

  empresas: Empresa[];

  responsable: Responsable = {
    "idResponsable": "",
    "nombre": "",
    "apellidos": "",
    "idEmpresaAux": "",
  };

  sub: Subscription;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private responsableService: ResponsableService,
    private empresaService: EmpresaService) { }

  ngOnInit() {
    this.empresaService.getAll().subscribe(data => {
      this.empresas = data;
    });
    
    this.sub = this.route.params.subscribe(params => {
      const id = params['id'];

     if(id){
       this.responsableService.get(id).subscribe((responsable: any) => {
         if(responsable){
           this.responsable = responsable;
         }
         else{
           console.log('El ciclo no existe');
           this.gotoList();
         }
       });
     }
   });
  }

  gotoList(){
    this.router.navigate(['/lista-responsables']);
  }

  createResponsable(): void{

    if(this.responsable.nombre == ""|| this.responsable.apellidos == ""){
        alert("Rellena todo el formulario");

        console.log(this.responsable.nombre);
        console.log(this.responsable.apellidos);
    }else{
      this.responsableService.createResponsable(this.responsable).subscribe( result => {
        this.gotoList();
      });
    }
  }

}
