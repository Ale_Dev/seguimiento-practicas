import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { saveAs } from 'file-saver';

import { Actividades } from '../models/actividades.model'

import { ActividadesService } from '../actividades.service';

@Component({
  selector: 'app-actividades-edit',
  templateUrl: './actividades-edit.component.html',
  styleUrls: ['./actividades-edit.component.css']
})
export class ActividadesEditComponent implements OnInit {

  sub: Subscription;
  
  listaActividades: Actividades[];

  id: string;

  lunes: Actividades = {
    "idActividades": "",
    "dia": "Lunes",
    "actividades": "",
    "tiempoEmpleado": "",
    "observaciones": "",
    "idSeguimientoAux": ""
  }

  martes: Actividades = {
    "idActividades": "",
    "dia": "Martes",
    "actividades": "",
    "tiempoEmpleado": "",
    "observaciones": "",
    "idSeguimientoAux": ""
  }

  miercoles: Actividades = {
    "idActividades": "",
    "dia": "Miercoles",
    "actividades": "",
    "tiempoEmpleado": "",
    "observaciones": "",
    "idSeguimientoAux": ""
  }

  jueves: Actividades = {
    "idActividades": "",
    "dia": "Jueves",
    "actividades": "",
    "tiempoEmpleado": "",
    "observaciones": "",
    "idSeguimientoAux": ""
  }

  viernes: Actividades = {
    "idActividades": "",
    "dia": "Viernes",
    "actividades": "",
    "tiempoEmpleado": "",
    "observaciones": "",
    "idSeguimientoAux": ""
  }

  constructor(private route: ActivatedRoute,
    private router: Router, private actividadService: ActividadesService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];

      this.lunes.idSeguimientoAux = this.id;
      this.martes.idSeguimientoAux = this.id;
      this.miercoles.idSeguimientoAux = this.id;
      this.jueves.idSeguimientoAux = this.id;
      this.viernes.idSeguimientoAux = this.id;

      if(this.id){
        this.actividadService.getAllByIdSeguimiento(this.id).subscribe((actividades: any) => {
          if(actividades){
            this.listaActividades = actividades;
          
            for(let i in this.listaActividades){
              switch(i){
                case "0":
                  this.lunes = this.listaActividades[i];
                  break;
                case "1":
                  this.martes = this.listaActividades[i];
                  break;
                case "2":
                  this.miercoles = this.listaActividades[i];
                  break;
                case "3":
                  this.jueves = this.listaActividades[i];
                  break;
                case "4":
                  this.viernes = this.listaActividades[i];
                  break;
              }
            }



          }
        });
      }
    });
  }

  guardarActividades(){
    this.actividadService.createActividad(this.lunes).subscribe(result => {
      this.actividadService.createActividad(this.martes).subscribe(result => {
        this.actividadService.createActividad(this.miercoles).subscribe(result => {
          this.actividadService.createActividad(this.jueves).subscribe(result => {
            this.actividadService.createActividad(this.viernes).subscribe(result => {
              console.log("Creado");
            });
          });
        });
      });
    });
    
    
    
    
  }

  getPdf(id){
    this.actividadService.pdfFull(id).subscribe(
      data => saveAs(data, 'Seguimiento Practicas '+id),
      error => console.error(error));
  }

}
