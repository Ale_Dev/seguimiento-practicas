import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

import { Admin } from './models/admin.model';

@Injectable()
export class AdminService {

  public API = '//localhost:8080';
  public ADMIN_API = this.API + '/admin';

  constructor(private http: HttpClient) { }

  getAll(){
    return this.http.get<Admin[]>(this.ADMIN_API + '/all');
  }
}
