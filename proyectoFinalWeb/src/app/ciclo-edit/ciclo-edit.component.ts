import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';

//Servicio Ciclos
import { CicloService } from '../ciclo.service';

//Clase Ciclo
import { Ciclo } from '../models/ciclo.model';

@Component({
  selector: 'app-ciclo-edit',
  templateUrl: './ciclo-edit.component.html',
  styleUrls: ['./ciclo-edit.component.css']
})
export class CicloEditComponent implements OnInit {

  ciclo: Ciclo = {
    "idCiclo": "",
    "nombre": "",
    "siglas": "",
    "tutor": ""
  };

  

  sub: Subscription;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private cicloService: CicloService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
       const id = params['id'];

      if(id){
        this.cicloService.get(id).subscribe((ciclo: any) => {
          if(ciclo){
            this.ciclo = ciclo;
          }
          else{
            console.log('El ciclo no existe');
            this.gotoList();
          }
        });
      }
    });
  }

  gotoList(){
    this.router.navigate(['/lista-ciclos']);
  }

  createCiclo(): void{
    
    if(this.ciclo.nombre == "" || this.ciclo.siglas == "" || this.ciclo.tutor == ""){
      alert("Rellena todo el formulario");
    }else{
      this.cicloService.createCiclo(this.ciclo).subscribe( result => {
        this.gotoList();
      });
    }
    
  }

}
