import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeguimientoAlumnoComponent } from './seguimiento-alumno.component';

describe('SeguimientoAlumnoComponent', () => {
  let component: SeguimientoAlumnoComponent;
  let fixture: ComponentFixture<SeguimientoAlumnoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeguimientoAlumnoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeguimientoAlumnoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
