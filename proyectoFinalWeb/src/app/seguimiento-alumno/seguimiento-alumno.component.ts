import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { saveAs } from 'file-saver';

import { SeguimientoService } from '../seguimiento.service';
import { Seguimiento } from '../models/seguimiento.model';
import { constructDependencies } from '@angular/core/src/di/reflective_provider';

import { ActividadesService } from '../actividades.service';

@Component({
  selector: 'app-seguimiento-alumno',
  templateUrl: './seguimiento-alumno.component.html',
  styleUrls: ['./seguimiento-alumno.component.css']
})
export class SeguimientoAlumnoComponent implements OnInit {

  idAlumno;

  sub: Subscription;

  seguimiento: Seguimiento = new Seguimiento();

  seguimientos: Seguimiento[];

  nuevoSeguimiento: any = {
    "inicio": "",
    "final": ""
  };

  constructor(private seguimientoService: SeguimientoService,
    private route: ActivatedRoute,
    private router: Router, 
    private actividadesService:ActividadesService) { }

  ngOnInit() {

    this.sub = this.route.params.subscribe(params => {
      this.idAlumno = params['id'];
      if(this.idAlumno){
        this.getSeguimientos(this.idAlumno);
      }
    });
  }

  getSeguimientos(id: string){
    this.seguimientoService.getByAlumno(id).subscribe(seguimientos => {
      this.seguimientos = seguimientos;
    });
  }

  addSeguimiento(){

    let yearActual = (new Date()).getFullYear();

    let inicioSplit: number[];
    let finalSplit: number[];

    if(this.nuevoSeguimiento.inicio == "" || this.nuevoSeguimiento.final == ""){
      alert("Introduce una fecha");
    }else{

      /**
       * Split[0]: Año
       * Split[1]: Mes
       * Split[2]: Dia
       */
      inicioSplit = this.nuevoSeguimiento.inicio.split("-");
      finalSplit = this.nuevoSeguimiento.final.split("-");

      if(inicioSplit[0]!=yearActual || finalSplit[0]!=yearActual || finalSplit[1]<inicioSplit[1]){
        alert("Introduce una fecha coherente");
      }else{
        if(inicioSplit[1] == finalSplit[1] || inicioSplit[1] < finalSplit[1]){
          if(inicioSplit[1] < finalSplit[1]){
            this.generarSeguimiento(inicioSplit, finalSplit);
          }else{
            if(inicioSplit[2]>finalSplit[2]){
              alert("Introduce una fecha coherente");
            }else{
              this.generarSeguimiento(inicioSplit, finalSplit);
            }
          }
        }
      }
    }
  }

  generarSeguimiento(inicioSplit, finalSplit){
    this.seguimiento.diaInicio = inicioSplit[2].toString();
            this.seguimiento.diaFinal = finalSplit[2].toString();

            switch(finalSplit[1].toString()){
              case "01":
                this.seguimiento.mes = "Enero";
                break;
              
              case "02":
                this.seguimiento.mes = "Febrero";
                break;

              case "03":
                this.seguimiento.mes = "Marzo";
                break;

              case "04":
                this.seguimiento.mes = "Abril";
                break;

              case "05":
                this.seguimiento.mes = "Mayo";
                break;

              case "06":
                this.seguimiento.mes = "Junio";
                break;

              case "07":
                this.seguimiento.mes = "Julio";
                break;

              case "08":
                this.seguimiento.mes = "Agosto";
                break;

              case "09":
                this.seguimiento.mes = "Septiembre";
                break;

              case "10":
                this.seguimiento.mes = "Octubre";
                break;

              case "11":
                this.seguimiento.mes = "Noviembre";
                break;

              case "12":
                this.seguimiento.mes = "Diciembre";
                break;
            }

            this.seguimiento.anio = finalSplit[0].toString();

            this.seguimiento.idAlumnoAux = this.idAlumno;

            console.log(this.seguimiento.diaInicio + " " + this.seguimiento.diaFinal + " " + 
              this.seguimiento.mes + " " + this.seguimiento.anio + " " + 
              this.seguimiento.idAlumnoAux);

              this.seguimientoService.createSeguimiento(this.seguimiento).subscribe(result => {
                this.seguimientoService.getAll().subscribe(segs => {
                  this.seguimientos = segs;
                });
              });
  }

  borrarSeguimiento(seguimiento: Seguimiento){

    this.actividadesService.deleteActividades(seguimiento.idSeguimiento).subscribe(result => {
      this.seguimientoService.deleteSeguimiento(seguimiento).subscribe(data => {
        this.seguimientos = this.seguimientos.filter(e => e!== seguimiento);
      });
    });
  }

  getPdfNull(){
    this.seguimientoService.pdfNull().subscribe(
      data => saveAs(data, 'Seguimiento Practicas Vacio'),
      error => console.error(error));
  }

}
