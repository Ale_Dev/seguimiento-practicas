import { Router } from '@angular/router';

import { Component, OnInit } from '@angular/core';
import { AlumnoService } from '../alumno.service';
import { AdminService } from '../admin.service';
import { Alumno } from '../models/alumno.model';
import { Admin } from '../models/admin.model';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit {

  alumos: Alumno[];
  admins: Admin[];

  alumno: Alumno = {
    "idAlumno": " ",
    "nombre": " ",
    "apellidos": " ",
    "email": " ",
    "pass": "",
    "idResponsableAux": " ",
    "idCicloAux": " ",
  }

  constructor(private alumnoService: AlumnoService, 
  private adminService: AdminService, 
  private router: Router) { }

  ngOnInit() {
    this.alumnoService.getAll().subscribe(alumnos => {
      this.alumos = alumnos;
    });

    this.adminService.getAll().subscribe(admins => {
      this.admins = admins;
    });
  }

  iniciarSesion(){

    let sesionIniciada = false;

    if(this.alumno.email == " " || this.alumno.pass == ""){
      alert("Rellena el formulario");
    }else{
      for(let i in this.alumos){
        let alumnoActual = this.alumos[i];

        if(this.alumno.email == alumnoActual.email &&
           this.alumno.pass == alumnoActual.pass){
             this.alumno = alumnoActual;
             sesionIniciada = true;
           }
      }

      if(!sesionIniciada){
        for(let admin of this.admins){
          if(admin.username == this.alumno.email && admin.pass == this.alumno.pass){
            sesionIniciada = true;
            this.router.navigate(['/lista-alumnos']);
          }
        }

        if(!sesionIniciada){
          alert("E-Mail o Contraseña incorrectas");
        }

      }else{
        this.router.navigate(['/seguimiento/'+this.alumno.idAlumno]);
      }
    }
  }

}
