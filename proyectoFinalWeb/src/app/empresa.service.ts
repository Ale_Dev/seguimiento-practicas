import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { Empresa } from './models/empresa.model';

@Injectable()
export class EmpresaService {

  public API = '//localhost:8080';
  public EMPRESA_API = this.API + '/empresas';

  //un objeto de la clase HttpClient para poder mandar peticiones a la API REST
  constructor(private http: HttpClient) { }

  get(id: string){
    return this.http.get(this.EMPRESA_API + '/'+id);
  }

  //llama al servicio de la API REST que consulta todas las empresas de la BBDD
  getAll(): Observable<any>{
    return this.http.get(this.EMPRESA_API + '/all');
  }

  createEmpresa(empresa){
    return this.http.post<Empresa>(this.EMPRESA_API + '/add', empresa);
  }

  public deleteEmpresa(empresa: Empresa){
    return this.http.delete(this.EMPRESA_API + '/delete/' + empresa.idEmpresa);
  }

}
