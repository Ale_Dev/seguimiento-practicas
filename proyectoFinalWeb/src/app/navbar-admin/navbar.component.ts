import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  
  ubicacion: string;
  rutaSeparada: string[];

  constructor(private router: Router) { }

  ngOnInit() {
  }

  comprobarlogIn(): boolean{
    this.ubicacion = this.router.url;
    
    this.rutaSeparada = this.ubicacion.split('/');

    if(this.rutaSeparada[1] == 'log-in'){
      return true;
    }else{
      return false;
    }
  }

  comprobarSeguimiento(): boolean{
    this.ubicacion = this.router.url;

    this.rutaSeparada = this.ubicacion.split('/');
    
    if(this.rutaSeparada[1] == 'seguimiento'){
      return true;
    }else{
      return false;
    }
  }

  comprobarActividades(): boolean{
    this.ubicacion = this.router.url;

    this.rutaSeparada = this.ubicacion.split('/');

    if(this.rutaSeparada[1]=='actividades'){
      return true;
    }else{
      return false;
    }
  }

}
