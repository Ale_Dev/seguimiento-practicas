import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaResponsablesComponent } from './lista-responsables.component';

describe('ListaResponsablesComponent', () => {
  let component: ListaResponsablesComponent;
  let fixture: ComponentFixture<ListaResponsablesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaResponsablesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaResponsablesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
