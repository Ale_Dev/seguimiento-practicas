import { Component, OnInit } from '@angular/core';

import { ResponsableService } from '../responsable.service';
import { Responsable } from '../models/responsable.model';

import { EmpresaService } from '../empresa.service';
import { Empresa } from '../models/empresa.model';

@Component({
  selector: 'app-lista-responsables',
  templateUrl: './lista-responsables.component.html',
  styleUrls: ['./lista-responsables.component.css']
})
export class ListaResponsablesComponent implements OnInit {

  responsables: Responsable[];
  empresas: Empresa[];

  constructor(private responsableService: ResponsableService,
    private empresaService: EmpresaService) { }

  ngOnInit() {
    this.responsableService.getAll().subscribe(data => {
      this.responsables = data;
    });

    this.empresaService.getAll().subscribe(data => {
      this.empresas = data;
    })
    
  }

  mostrarEmpresa(idEmpresa): string{
    let nombreEmpresa = " ";

    for(let i in this.empresas){
      let empresa = this.empresas[i];

      if(idEmpresa == empresa.idEmpresa){
        nombreEmpresa = empresa.nombre;
      }else{
        nombreEmpresa = nombreEmpresa;
      }
    }

    return nombreEmpresa;
  }

  deleteResponsable(responsable: Responsable){
    this.responsableService.deleteResponsable(responsable).subscribe(data => {
      this.responsables = this.responsables.filter(r => r!== responsable);
    })
  }
}
