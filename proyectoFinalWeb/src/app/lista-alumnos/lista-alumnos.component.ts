import { Component, OnInit } from '@angular/core';

//importa todos los servicios HTTP
import { AlumnoService } from '../alumno.service';
import { CicloService } from '../ciclo.service';
import { EmpresaService } from '../empresa.service';
import { ResponsableService } from '../responsable.service';
import { ActividadesService } from '../actividades.service';
import { SeguimientoService } from '../seguimiento.service';

//importa las clases con los modelos de Alumno, Ciclo y Empresa
import { Alumno } from '../models/alumno.model';
import { Ciclo } from '../models/ciclo.model';
import { Empresa } from '../models/empresa.model';
import { Responsable } from '../models/responsable.model';
import { Seguimiento } from '../models/seguimiento.model';
import { forEach } from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-lista-alumnos',
  templateUrl: './lista-alumnos.component.html',
  styleUrls: ['./lista-alumnos.component.css'],
  providers: [AlumnoService]
})
export class ListaAlumnosComponent implements OnInit {

  //array de objetos de los tipos correspondientes, alumno, ciclo y empresa respectivamente
  alumnos: Alumno[];
  ciclos: Ciclo[];
  empresas: Empresa[];
  responsables: Responsable[];
  listaResponsables: Responsable[];
  seguimientos: Seguimiento[];

  //objeto de tipo AlumnoService, donde se encuentran las llamadas a la API REST
  constructor(private alumnoService: AlumnoService,
     private cicloService: CicloService, private empresaService: EmpresaService,
     private responsableService: ResponsableService, 
     private seguimientoService: SeguimientoService,
     private actividadesService: ActividadesService) { }

  //operaciones que realiza al iniciar el componente
  ngOnInit() {
    //consulta la base de datos y guarda todos los alumnos en el Array de tipo Alumno
    this.alumnoService.getAll().subscribe(data => {
      this.alumnos = data;
    });

    //consulta la base de datos y guarda todos los ciclos en un Array de tipo Ciclo
    this.cicloService.getAll().subscribe(data => {
      this.ciclos = data;
    })

    //consulta la base de datos y guarda todas las empresas en un Array de tipo Empresa
    this.empresaService.getAll().subscribe(data => {
      this.empresas = data;
    });

    //consulta todos los responsables
    this.responsableService.getAll().subscribe(data => {
      this.responsables = data;
    });
  }

  deleteAlumno(alumno: Alumno): void{

    
    this.seguimientoService.getByAlumno(alumno.idAlumno).subscribe(seg => {
      this.seguimientos = seg;

      if(seg.length == 0){
        this.alumnoService.deleteAlumno(alumno).subscribe(data => {
          this.alumnos = this.alumnos.filter(al => al!== alumno);
        });
      }else{
        for(let seguimiento of this.seguimientos){
          console.log(this.seguimientos.length);
          this.actividadesService.deleteActividades(seguimiento.idSeguimiento).subscribe(result =>{
            this.seguimientoService.deleteSeguimiento(seguimiento).subscribe(data => {
              this.alumnoService.deleteAlumno(alumno).subscribe(data => {
                this.alumnos = this.alumnos.filter(a => a!== alumno);
              });
            });
          });
        }
      } 
    });
  }

  mostrarCiclo(idCiclo: string): string{

    let siglasCiclo = " ";

    for(let i in this.ciclos){
      let ciclo = this.ciclos[i];

      if(ciclo.idCiclo == idCiclo){
        siglasCiclo = ciclo.siglas;
      }else{
        siglasCiclo = siglasCiclo;
      }
    }
    return siglasCiclo;
  }

  mostrarEmpresa(idResponsable: string): string{

    let idEmpresa = "";
    let nombreEmpresa = "";

    for(let i in this.responsables){

      let responsable = this.responsables[i];
  
      if(idResponsable == responsable.idResponsable){
        idEmpresa = responsable.idEmpresaAux;
      }else{
        idEmpresa = idEmpresa;
      }
    }

    for(let j in this.empresas){

      let empresa = this.empresas[j];

      if(idEmpresa == empresa.idEmpresa){
        nombreEmpresa = empresa.nombre;
      }else{
        nombreEmpresa = nombreEmpresa;
      }
    }

    return nombreEmpresa;

  }

  mostrarResponsable(idResponsable): string{
    let nombreResponsable = " ";

    for(let i in this.responsables){
      let responsable = this.responsables[i];

      if(idResponsable == responsable.idResponsable){
        nombreResponsable = responsable.nombre + ' ' + responsable.apellidos;
      }else{
        nombreResponsable = nombreResponsable;
      }
    }

    return nombreResponsable;
  }

  mostrarListaResponsables(idEmpresa: string){
    let responsable = document.getElementById("Responsable");

    this.responsableService.getByEmpresa(idEmpresa).subscribe(data => {
      this.listaResponsables = data;
    });

    responsable.removeAttribute("disabled");
  }

  buscarPorCiclo(idCiclo: string){
    this.alumnoService.getByCiclo(idCiclo).subscribe(data => {
      this.alumnos = data;
    });
  }

  buscarPorResponsable(idResponsable: string){
    this.alumnoService.getByResponsable(idResponsable).subscribe(data => {
      this.alumnos = data;
    });
  }
}
