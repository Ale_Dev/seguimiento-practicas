import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaCiclosComponent } from './lista-ciclos.component';

describe('ListaCiclosComponent', () => {
  let component: ListaCiclosComponent;
  let fixture: ComponentFixture<ListaCiclosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaCiclosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaCiclosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
