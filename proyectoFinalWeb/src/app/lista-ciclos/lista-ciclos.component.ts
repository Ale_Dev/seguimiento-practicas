import { Component, OnInit } from '@angular/core';

import { CicloService } from '../ciclo.service';
import { Ciclo } from '../models/ciclo.model';

@Component({
  selector: 'app-lista-ciclos',
  templateUrl: './lista-ciclos.component.html',
  styleUrls: ['./lista-ciclos.component.css']
})
export class ListaCiclosComponent implements OnInit {

  ciclos: Ciclo[];

  constructor(private cicloService: CicloService) { }

  ngOnInit() {
    this.cicloService.getAll().subscribe(data => {
      this.ciclos = data;
    });
  }

  deleteCiclo(ciclo: Ciclo): void{
    this.cicloService.deleteCiclo(ciclo).subscribe(data => {
      this.ciclos = this.ciclos.filter(c => c!== ciclo);
    })
  }

}
