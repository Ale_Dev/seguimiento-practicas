import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';

import {EmpresaService } from '../empresa.service';

import { Empresa } from '../models/empresa.model';

@Component({
  selector: 'app-empresa-edit',
  templateUrl: './empresa-edit.component.html',
  styleUrls: ['./empresa-edit.component.css']
})
export class EmpresaEditComponent implements OnInit {

  empresa: Empresa = {
    "idEmpresa": "",
    "nombre": "",
    "direccion1": "",
    "telefono1": ""
  };

  sub: Subscription;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private empresaService: EmpresaService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      const id = params['id'];

      if(id){
        this.empresaService.get(id).subscribe((empresa: any) => {
          if(empresa){
            this.empresa = empresa;
          }
          else{
            console.log('La empresa no existe');
            this.gotoList();
          }
        });
      }
    });
  }

  gotoList(){
    this.router.navigate(['/lista-empresas']);
  }

  createEmpresa(): void{
    if(this.empresa.nombre == "" || this.empresa.direccion1 == "" || this.empresa.telefono1 == ""
     || !Number.isInteger(Number.parseInt(this.empresa.telefono1))){
      alert("Rellena todo el formulario correctamente");
    }else{
      this.empresaService.createEmpresa(this.empresa).subscribe( result => {
        this.gotoList();
      });
    }
  }

}
