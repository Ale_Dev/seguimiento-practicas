export class Alumno {

    idAlumno: string;
    nombre: string;
    apellidos: string;
    email: string;
    pass: string;
    idResponsableAux: string;
    idCicloAux: string;
}