export class Responsable {

    idResponsable: string;
    nombre: string;
    apellidos: string;
    idEmpresaAux: string;
}