import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

import { Seguimiento }from './models/seguimiento.model';

@Injectable()
export class SeguimientoService {

  public API = '//localhost:8080';
  public SEGUIMIENTO_API = this.API + '/seguimientos';

  constructor(private http: HttpClient) { }

  getAll(): Observable<any> {
    return this.http.get<Seguimiento[]>(this.SEGUIMIENTO_API + '/all');
  }

  getByAlumno(id: string){
    return this.http.get<Seguimiento[]>(this.SEGUIMIENTO_API + '/alumno/' + id);
  }

  createSeguimiento(seguimiento){
    return this.http.post<Seguimiento>(this.SEGUIMIENTO_API + '/add', seguimiento);
  }

  public deleteSeguimiento(seguimiento: Seguimiento){
    return this.http.delete(this.SEGUIMIENTO_API + '/delete/' + seguimiento.idSeguimiento);
  }

  public pdfNull(){
    return this.http.get(this.API + "/pdf/null", { responseType: 'blob', 
    headers: new HttpHeaders().append('ContentType','application/json')
    });
  }

}
