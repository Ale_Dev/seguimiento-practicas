import { Component, OnInit } from '@angular/core';

import { Empresa } from '../models/empresa.model';
import { EmpresaService } from '../empresa.service';

@Component({
  selector: 'app-lista-empresas',
  templateUrl: './lista-empresas.component.html',
  styleUrls: ['./lista-empresas.component.css']
})
export class ListaEmpresasComponent implements OnInit {

  empresas: Empresa[];
  
  constructor(private empresaService: EmpresaService) { }

  ngOnInit() {
    this.empresaService.getAll().subscribe(data => {
      this.empresas = data;
    })
   
  }

  deleteEmpresa(empresa: Empresa){
    this.empresaService.deleteEmpresa(empresa).subscribe(data => {
      this.empresas = this.empresas.filter(e => e!== empresa);
    })
  }

}
