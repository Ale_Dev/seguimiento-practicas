//imports Angular
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';

//Componentes o Servicios de Alumno
import { AlumnoService } from './alumno.service';
import { ListaAlumnosComponent } from './lista-alumnos/lista-alumnos.component';
import { AlumnoEditComponent } from './alumno-edit/alumno-edit.component';

//Componentes o Servicios de Ciclo
import { CicloService } from './ciclo.service';

//Componentes o Servicios de Empresa
import { EmpresaService } from './empresa.service';
import { NavbarComponent } from './navbar-admin/navbar.component';
import { ListaCiclosComponent } from './lista-ciclos/lista-ciclos.component';
import { CicloEditComponent } from './ciclo-edit/ciclo-edit.component';
import { ListaEmpresasComponent } from './lista-empresas/lista-empresas.component';
import { EmpresaEditComponent } from './empresa-edit/empresa-edit.component';
import { ListaResponsablesComponent } from './lista-responsables/lista-responsables.component';
import { ResponsableEditComponent } from './responsable-edit/responsable-edit.component';
import { ResponsableService } from './responsable.service';
import { LogInComponent } from './log-in/log-in.component';
import { SeguimientoAlumnoComponent } from './seguimiento-alumno/seguimiento-alumno.component';

//Componentes o Servicios de Seguimiento
import { SeguimientoService } from './seguimiento.service';
import { ActividadesEditComponent } from './actividades-edit/actividades-edit.component';
import { ActividadesService } from './actividades.service';
import { AdminService } from './admin.service';

//rutas para en HTML redireccionar con [routerLink]
const appRoutes: Routes = [
  { 
    path: '', redirectTo: '/log-in', 
    pathMatch: 'full' 
  },
  {
    path: 'log-in',
    component: LogInComponent
  },
  {
    path: 'lista-alumnos',
    component: ListaAlumnosComponent
  },
  {
    path: 'alumno-add',
    component: AlumnoEditComponent
  },
  {
    path: 'alumno-edit/:id',
    component: AlumnoEditComponent
  },
  {
    path: 'lista-ciclos',
    component: ListaCiclosComponent
  },
  {
    path: 'ciclo-add',
    component: CicloEditComponent
  },
  {
    path: 'ciclo-edit/:id',
    component: CicloEditComponent
  },
  {
    path:'lista-empresas',
    component: ListaEmpresasComponent
  },
  {
    path: 'empresa-add',
    component: EmpresaEditComponent
  },
  {
    path: 'empresa-edit/:id',
    component: EmpresaEditComponent
  },
  {
    path: 'lista-responsables',
    component: ListaResponsablesComponent
  },
  {
    path: 'responsable-add',
    component: ResponsableEditComponent
  },
  {
    path: 'responsable-edit/:id',
    component: ResponsableEditComponent
  },
  {
    path: 'seguimiento/:id',
    component: SeguimientoAlumnoComponent
  },
  {
    path: 'actividades/:id',
    component: ActividadesEditComponent
  }
];

//donde aparecen todos los compenentes e imports que utiliza Angular para esta App
@NgModule({
  declarations: [
    AppComponent,
    ListaAlumnosComponent,
    AlumnoEditComponent,
    NavbarComponent,
    ListaCiclosComponent,
    CicloEditComponent,
    ListaEmpresasComponent,
    EmpresaEditComponent,
    ListaResponsablesComponent,
    ResponsableEditComponent,
    LogInComponent,
    SeguimientoAlumnoComponent,
    ActividadesEditComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  //Permiten que estos servicios puedan recoger informacion
  providers: [AlumnoService, CicloService, EmpresaService, ResponsableService, 
    SeguimientoService, ActividadesService, AdminService],
  bootstrap: [AppComponent]
})
export class AppModule { }
