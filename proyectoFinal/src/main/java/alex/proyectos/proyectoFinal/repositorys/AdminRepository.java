package alex.proyectos.proyectoFinal.repositorys;

import alex.proyectos.proyectoFinal.modelos.Admin;
import org.springframework.data.repository.CrudRepository;

/**
 * Repositorio para los Administradores
 * @author alex
 */
public interface AdminRepository extends CrudRepository<Admin, Integer>{
}
