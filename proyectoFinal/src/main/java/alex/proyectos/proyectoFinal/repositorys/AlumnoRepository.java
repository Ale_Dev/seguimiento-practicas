package alex.proyectos.proyectoFinal.repositorys;

import alex.proyectos.proyectoFinal.modelos.Alumno;
//import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

/**
 * Repositorio para los alumnos
 * @author alex
 */
@RepositoryRestResource
@CrossOrigin(origins = "http://localhost:4200")//permite a localhost de Angular conectar a la REST API
public interface AlumnoRepository extends CrudRepository<Alumno, Integer> {

    /**
     * Busca alumnos del mismo ciclo
     * @param idCiclo
     * @return
     */
    Iterable<Alumno> findByIdCicloAux(Integer idCiclo);

    /**
     * Busca alumnos de un mismo responsable
     * @param idResponsable
     * @return
     */
    Iterable<Alumno> findByIdResponsableAux(Integer idResponsable);

}
