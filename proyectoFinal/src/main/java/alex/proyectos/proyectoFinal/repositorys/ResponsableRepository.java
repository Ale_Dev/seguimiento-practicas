package alex.proyectos.proyectoFinal.repositorys;

import alex.proyectos.proyectoFinal.modelos.Responsable;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.Optional;

/**
 * Repositorio para los Responsables
 * @author alex
 */
@RepositoryRestResource
@CrossOrigin(origins = "http://localhost:4200")//permite a localhost de Angular conectar a la REST API
public interface ResponsableRepository extends CrudRepository<Responsable, Integer>{

    /**
     * Busca los responsables que pertenezcan a la misma empresa
     * @param idEmpresa
     * @return
     */
    Iterable<Responsable>findByIdEmpresaAux(Integer idEmpresa);
}
