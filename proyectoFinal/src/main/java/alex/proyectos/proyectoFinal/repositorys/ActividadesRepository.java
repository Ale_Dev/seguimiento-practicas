package alex.proyectos.proyectoFinal.repositorys;

import alex.proyectos.proyectoFinal.modelos.Actividades;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

/**
 * Repositorio para las actividades
 * @author alex
 */
@RepositoryRestResource
@CrossOrigin(origins = "http://localhost:4200")//permite a localhost de Angular conectar a la REST API
public interface ActividadesRepository extends CrudRepository<Actividades, Integer> {

    /**
     * Busca las actividades que pertenezcan a una misma hoja de seguimiento
     * @param idSeguimientoAux
     * @return
     */
    Iterable<Actividades> findByIdSeguimientoAux(Integer idSeguimientoAux);

    /**
     * Borra todas las actividades que pertenezcan a la misma hoja de seguimiento
     * @param idSeguimientoAux
     */
    void deleteByIdSeguimientoAux(Integer idSeguimientoAux);
}
