package alex.proyectos.proyectoFinal.controllers;

import alex.proyectos.proyectoFinal.modelos.Seguimiento;
import alex.proyectos.proyectoFinal.repositorys.SeguimientoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;

/**
 * @author alex
 * Realiza las consultas a la BBDD relacionadas con los Seguimientos que seran necesarias
 */

@Controller
@RequestMapping(path = "/seguimientos")//URL principal
@CrossOrigin(origins = "http://localhost:8100")
public class SeguimientoController {

    //interfaz repositorio que hace tiene los metodos del CRUD
    @Autowired
    private SeguimientoRepository repository;

    /**
     * Consulta todos los Seguimientos de la BBDD
     * @return Iterable<Seguimiento>
     */
    @GetMapping(path = "/all")//URL a la que se accede para hacer la consulta a la BBDD
    @CrossOrigin(origins = "http://localhost:4200")//permite a Angular conectar a la REST API
    public @ResponseBody Iterable<Seguimiento>getSeguimientos(){
        return this.repository.findAll();
    }

    /**
     * Busca todos los Seguimientos que pertenecen a un Alumno concreto
     * @param idAlumno
     * @return Iterable<Seguimiento>
     */
    @GetMapping(path = "/alumno/{id}")
    @CrossOrigin(origins = "http://localhost:4200")
    public @ResponseBody Iterable<Seguimiento>getSeguimientoByAlumno(@PathVariable("id") Integer idAlumno){
        return repository.findByIdAlumnoAux(idAlumno);
    }

    /**
     * Crea un seguimiento
     * @param seguimiento
     * @return Seguimiento
     */
    @PostMapping(path = "/add")
    @CrossOrigin(origins = "http://localhost:4200")
    public @ResponseBody Seguimiento crearSeguimiento(@RequestBody Seguimiento seguimiento){
        return repository.save(seguimiento);
    }

    /**
     * Borra un Seguimiento con un ID determinado
     * @param id
     */
    @DeleteMapping(path = {"/delete/{id}"})
    @CrossOrigin(origins = "http://localhost:4200")
    @Transactional
    public @ResponseBody
    void borrarSeguimiento(@PathVariable("id")Integer id){
        repository.deleteById(id);
    }
}
