package alex.proyectos.proyectoFinal.controllers;

import alex.proyectos.proyectoFinal.modelos.Actividades;
import alex.proyectos.proyectoFinal.repositorys.ActividadesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;


/**
 *@author alex
 *Realiza las consultas a la BBDD relacionadas con las Actividades que seran necesarias
 */

@Controller
@RequestMapping(path = "/actividades")//URL principal
@CrossOrigin(origins = "http://localhost:8100")
public class ActividadesController {

    //interfaz repositorio que hace tiene los metodos del CRUD
    @Autowired
    private ActividadesRepository repository;

    /**
     * Consulta todas las actividades de la BBDD
     * @return Iterable<Actividades>
     */
    @GetMapping(path = "/all")
    @CrossOrigin(origins = {"http://localhost:4200", ""})
    public @ResponseBody Iterable<Actividades>getActividades(){
        return repository.findAll();
    }

    /**
     * Busca todas las actividades de una misma hoja de seguimiento
     * @param idSeguimiento
     * @return  Iterable<Actividades>
     */
    @GetMapping(path = "/{id}")
    @CrossOrigin(origins = "http://localhost:4200")
    public @ResponseBody
    Iterable<Actividades>getActividadesByIdSeguimiento(@PathVariable("id")Integer idSeguimiento){
        return repository.findByIdSeguimientoAux(idSeguimiento);
    }

    /**
     * Crea una actividad
     * @param actividad
     * @return Actividades
     */
    @PostMapping(path = "/add")
    @CrossOrigin(origins = "http://localhost:4200")
    public @ResponseBody Actividades crearActividades(@RequestBody Actividades actividad){
        return repository.save(actividad);
    }

    /**
     * Borra una las actividades que pertenezcan a la misma hoja de seguimientos
     * @param idSeguimientoAux
     */
    @DeleteMapping(path = {"/delete/{id}"})
    @CrossOrigin(origins = "http://localhost:4200")
    @Transactional
    public @ResponseBody
    void borrarActividades(@PathVariable("id")Integer idSeguimientoAux){
        repository.deleteByIdSeguimientoAux(idSeguimientoAux);
    }
}
