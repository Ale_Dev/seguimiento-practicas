package alex.proyectos.proyectoFinal.controllers;


import alex.proyectos.proyectoFinal.modelos.Empresa;
import alex.proyectos.proyectoFinal.repositorys.EmpresaRepository;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

import javax.transaction.Transactional;

/**
 * @author alex
 * Realiza las consultas a la BBDD relacionadas con las Empresas que seran necesarias
 */

@Controller
@RequestMapping(path = "/empresas")//URL principal
public class EmpresaController {

    //interfaz repositorio que hace tiene los metodos del CRUD
    @Autowired
    private EmpresaRepository repository;

    /**
     * Consulta todas las empresas de la BBDD
     * @return Iterable<Empresa>
     */
    @GetMapping(path = "/all")//URL a la que se accede para hacer la consulta a la BBDD
    @CrossOrigin(origins = "http://localhost:4200")//permite a Angular conectar a la REST API
    public @ResponseBody Iterable<Empresa> getEmpresas(){
        return repository.findAll();
    }

    /**
     * Busca en la BBDD una empresa con un ID determniado
     * @param id
     * @return Optional<Empresa>
     */
    @GetMapping(path = "/{id}")
    @CrossOrigin(origins = "http://localhost:4200")
    public @ResponseBody
    Optional<Empresa>getEmpresa(@PathVariable("id") Integer id){
        return repository.findById(id);
    }

    /**
     * Crea una empresa
     * @param empresa
     * @return Empresa
     */
    @PostMapping(path = "/add")
    @CrossOrigin(origins = "http://localhost:4200")
    public @ResponseBody Empresa crearEmpresa(@RequestBody Empresa empresa){
        return repository.save(empresa);
    }

    /**
     * Borra una empresa con un ID determinado
     * @param id
     */
    @DeleteMapping(path = {"/delete/{id}"})
    @CrossOrigin(origins = "http://localhost:4200")
    @Transactional
    @OnDelete(action= OnDeleteAction.CASCADE)
    public @ResponseBody
    void borrarEmpresa(@PathVariable("id")Integer id){
        repository.deleteById(id);
    }
}
