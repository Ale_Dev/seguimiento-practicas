package alex.proyectos.proyectoFinal.controllers;

import alex.proyectos.proyectoFinal.modelos.Ciclo;
import alex.proyectos.proyectoFinal.repositorys.CicloRepository;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.persistence.ManyToOne;
import javax.transaction.Transactional;
import java.util.Optional;

/**
 * @author alex
 * Realiza las consultas a la BBDD relacionadas con los Ciclos que seran necesarias
 */
@Controller
@RequestMapping(path = "/ciclos")//URL principal
public class CicloController {

    //interfaz repositorio que hace tiene los metodos del CRUD
    @Autowired
    private CicloRepository repository;

    /**
     * Consulta todos los ciclos
     * @return Iterable<Ciclo>
     */
    @GetMapping(path = "/all")//URL a la que se accede para hacer la consulta a la BBDD
    @CrossOrigin(origins = "http://localhost:4200")//permite a Angular conectar a la REST API
    public @ResponseBody
    Iterable<Ciclo>getCiclos(){
        return repository.findAll();
    }

    /**
     * Busca un Ciclo por ID
     * @param id
     * @return Optional<Ciclo>
     */
    @GetMapping(path = "/{id}")
    @CrossOrigin(origins = "http://localhost:4200")
    public @ResponseBody
    Optional<Ciclo> getCiclo(@PathVariable("id") Integer id){
        return repository.findById(id);
    }

    /**
     * Crea un ciclo
     * @param ciclo
     * @return Ciclo
     */
    @PostMapping(path = "/add")
    @CrossOrigin(origins = "http://localhost:4200")
    public @ResponseBody Ciclo crearCiclo(@RequestBody Ciclo ciclo){
        return repository.save(ciclo);
    }

    /**
     * Borra un ciclo con un ID determinado
     * @param id
     */
    @DeleteMapping(path = {"/delete/{id}"})
    @OnDelete(action= OnDeleteAction.CASCADE)
    @CrossOrigin(origins = "http://localhost:4200")
    @Transactional
    public @ResponseBody
    void borrarCiclo(@PathVariable("id")Integer id){
        repository.deleteById(id);
    }



}
