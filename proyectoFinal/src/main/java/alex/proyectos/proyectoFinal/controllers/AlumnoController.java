package alex.proyectos.proyectoFinal.controllers;

import alex.proyectos.proyectoFinal.modelos.Alumno;
import alex.proyectos.proyectoFinal.repositorys.AlumnoRepository;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.Optional;

/**
 * @author alex
 * Realiza las consultas a la BBDD relacionadas con los Alumnos que seran necesarias
 */

@Controller
@RequestMapping(path = "/alumnos")//URL principal
@CrossOrigin(origins = "http://localhost:8100")
public class AlumnoController {

    //interfaz repositorio que hace tiene los metodos del CRUD
    @Autowired
    private AlumnoRepository repository;

    /**
     * Consulta la tabla alumno de la BBDD
     * @return Iterable<Alumno>
     */
    @GetMapping(path = "/all")//URL a la que se accede para hacer la consulta a la BBDD
    @CrossOrigin(origins = "http://localhost:4200")//permite a Angular conectar a la REST API
    public @ResponseBody Iterable<Alumno> getAlumnos(){
        return repository.findAll();
    }

    /**
     * Busca un alumno con un ID determinado
     * @param id
     * @return Optional<Alumno>
     */
    @GetMapping(path = "/{id}")
    @CrossOrigin(origins = "http://localhost:4200")
    public @ResponseBody
    Optional<Alumno> getAlumno(@PathVariable("id") Integer id){
        return repository.findById(id);
    }

    /**
     * Busca todos los alumnos que pertenezcan a un Ciclo determinado
     * @param id
     * @return Iterable<Alumno>
     */
    @GetMapping(path = "/ciclo/{id}")
    @CrossOrigin(origins = "http://localhost:4200")
    public @ResponseBody
    Iterable<Alumno> getAlumnoByCiclo(@PathVariable("id") Integer id){
        return repository.findByIdCicloAux(id);
    }

    /**
     * Busca todos los alumnos con el mismo responsable
     * @param id
     * @return Iterable<Alumno>
     */
    @GetMapping(path = "/responsable/{id}")
    @CrossOrigin(origins = "http://localhost:4200")
    public @ResponseBody
    Iterable<Alumno> getAlumnoByResponsable(@PathVariable("id") Integer id){
        return repository.findByIdResponsableAux(id);
    }

    /**
     * Crea un alumno
     * @param alumno
     * @return Alumno
     */
    @PostMapping(path = "/add")
    @CrossOrigin(origins = "http://localhost:4200")
    public @ResponseBody Alumno crearAlumno(@RequestBody Alumno alumno){
        return repository.save(alumno);
    }

    /**
     * Borra un alumno
     * @param id
     */
    @DeleteMapping(path = {"/delete/{id}"})
    @CrossOrigin(origins = "http://localhost:4200")
    @Transactional
    public @ResponseBody
    void borrarAlumno(@PathVariable("id")Integer id){
        repository.deleteById(id);
    }


}
