package alex.proyectos.proyectoFinal.controllers;

import alex.proyectos.proyectoFinal.modelos.*;
import alex.proyectos.proyectoFinal.repositorys.*;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfDocument;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

/**
 * @author alex
 * Genera el PDF para que el front pueda descargarlo, tanto vacio como relleno
 */

@Controller
@RequestMapping(path = "/pdf",method = RequestMethod.GET, produces = MediaType.APPLICATION_PDF_VALUE)//URL principal
public class PdfController {

    //Repositorios que van a ser necesarios para generar el PDF relleno
    @Autowired
    private ActividadesRepository ar;

    @Autowired
    private SeguimientoRepository sr;

    @Autowired
    private AlumnoRepository alr;

    @Autowired
    private ResponsableRepository rr;

    @Autowired
    private EmpresaRepository er;

    @Autowired
    private CicloRepository cr;

    /**
     * @return ResponseEntity<InputStreamResource>
     * @throws IOException
     */
    @GetMapping(path = "/null")
    @CrossOrigin(origins = "http://localhost:4200")
    public @ResponseBody
    ResponseEntity<InputStreamResource> getPDF() throws IOException {

        ByteArrayInputStream bis = createReport();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=migration.pdf");


        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }

    /**
     * @param idSeguimiento
     * @return ResponseEntity<InputStreamResource>
     * @throws IOException
     */
    @GetMapping(path = "/{id}")
    @CrossOrigin(origins = "http://localhost:4200")
    public @ResponseBody
    ResponseEntity<InputStreamResource> getPDFRelleno(@PathVariable("id") Integer idSeguimiento) throws IOException {

        ArrayList<Actividades> actividades = new ArrayList<Actividades>();

        Seguimiento seguimiento;
        Alumno alumno;
        Responsable responsable;
        Empresa empresa;
        Ciclo ciclo;

        ar.findByIdSeguimientoAux(idSeguimiento).forEach(actividades1 -> actividades.add(actividades1));

        seguimiento = sr.findById(actividades.get(0).getIdSeguimientoAux()).get();

        alumno = alr.findById(seguimiento.getIdAlumnoAux()).get();

        responsable = rr.findById(alumno.getIdResponsableAux()).get();

        empresa = er.findById(responsable.getIdEmpresaAux()).get();

        ciclo = cr.findById(alumno.getIdCicloAux()).get();

        ByteArrayInputStream bis = createReport(actividades, seguimiento, alumno, responsable, empresa, ciclo);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=migration.pdf");


        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }

    /**
     * Crea el Documento Vacio
     * @return ByteArrayInputStream
     */
    public static ByteArrayInputStream createReport() {


        Document document = new Document(PageSize.A4.rotate());
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {


            PdfWriter.getInstance(document, out);
            document.open();

            document.add(cabecera());
            document.add(spacing(2));
            document.add(semana());
            document.add(tablaInfoALumno());
            document.add(spacing(1));
            document.add(seguimientoPracticas());
            document.add(spacing(1));
            document.add(firmas());

            document.close();

        } catch (DocumentException ex) {

            System.out.println("error");
        }

        return new ByteArrayInputStream(out.toByteArray());
    }

    /**
     * Crea el documento relleno
     * @param actividades
     * @param seguimiento
     * @param alumno
     * @param responsable
     * @param empresa
     * @param ciclo
     * @return ByteArrayInputStream
     */
    public static ByteArrayInputStream createReport(ArrayList<Actividades> actividades, Seguimiento seguimiento, Alumno alumno,
                                                    Responsable responsable, Empresa empresa, Ciclo ciclo) {


        Document document = new Document(PageSize.A4.rotate());
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {


            PdfWriter.getInstance(document, out);
            document.open();

            document.add(cabecera());
            document.add(spacing(2));
            document.add(semana(seguimiento));
            document.add(tablaInfoALumno(alumno, responsable, empresa, ciclo));
            document.add(spacing(1));
            document.add(seguimientoPracticas(actividades));
            document.add(spacing(1));
            document.add(firmas());

            document.close();

        } catch (DocumentException ex) {

            System.out.println("error");
        }

        return new ByteArrayInputStream(out.toByteArray());
    }

    /**
     * Crea la cabecera del documento
     * @return la cabecera del documento
     */
    public static Paragraph cabecera() {
        Paragraph cabecera = new Paragraph();
        cabecera.setAlignment(Element.ALIGN_CENTER);
        cabecera.add("Anexo 6\n"
                + "FORMACION EN CENTROS DE TRABAJO\n"
                + "Ficha semanal del alumno\n"
                + "(Centros educativos de titularidad privada)");

        return cabecera;
    }

    public static Paragraph spacing(int espacios) {
        Paragraph spacing = new Paragraph();

        switch(espacios) {
            case 1:
                spacing.add("\n");
                break;

            case 2:
                spacing.add("\n\n");
                break;

            case 3:
                spacing.add("\n\n\n");
                break;
        }

        return spacing;
    }

    /**
     * Crea la tabla de la info de la semana vacia
     * @return PdfPTable
     */
    public static PdfPTable semana() {

        PdfPTable table = new PdfPTable(1);
        table.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.setWidthPercentage(50);

        PdfPCell cell;

        cell = new PdfPCell(new Phrase("Semana de .... a .... de ......................................... de ...."));
        //cell.setColspan(3);
        table.addCell(cell);

        return table;
    }

    /**
     * Crea la tabla de la info de la seamana rellena
     * @param seguimiento
     * @return PdfPTable
     */
    public static PdfPTable semana(Seguimiento seguimiento) {

        PdfPTable table = new PdfPTable(1);
        table.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.setWidthPercentage(50);

        PdfPCell cell;

        cell = new PdfPCell(new Phrase("Semana de "+seguimiento.getDiaInicio()+
                " a "+seguimiento.getDiaFinal()+" de "+seguimiento.getMes()+" de "+seguimiento.getAnio()));
        //cell.setColspan(3);
        table.addCell(cell);

        return table;
    }

    /**
     * Crea la tabla con la informacion del alumno vacia
     * @return PdfPTable
     */
    public static PdfPTable tablaInfoALumno() {

        // tabla con dos columnas
        PdfPTable table = new PdfPTable(2);
        table.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.setWidthPercentage(100);

        PdfPCell cell;

        cell = new PdfPCell(new Phrase("CENTRO DONCENTE: CES FUENCARRAL\n\nPROFESOR TUTOR:"));
        table.addCell(cell);
        cell = new PdfPCell(new Phrase("EMPRESA COLABORADORA: \n\nRESPONSABLE DE LA EMPRESA: "));
        table.addCell(cell);

        table.addCell("ALUMNO:");
        table.addCell("CICLO FORMATIVO:");
        return table;
    }

    /**
     * Crea la tabla con la informacion del alumno rellena
     * @param alumno
     * @param responsable
     * @param empresa
     * @param ciclo
     * @return PdfPTable
     */
    public static PdfPTable tablaInfoALumno(Alumno alumno, Responsable responsable, Empresa empresa, Ciclo ciclo) {

        // tabla con dos columnas
        PdfPTable table = new PdfPTable(2);
        table.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.setWidthPercentage(100);

        PdfPCell cell;

        cell = new PdfPCell(new Phrase("CENTRO DONCENTE: CES FUENCARRAL\n\nPROFESOR TUTOR: "+ciclo.getTutor()));
        table.addCell(cell);
        cell = new PdfPCell(new Phrase("EMPRESA COLABORADORA: "+ empresa.getNombre() +
                "\n\nRESPONSABLE DE LA EMPRESA: "+responsable.getNombre() +" "+ responsable.getApellidos()));
        table.addCell(cell);

        table.addCell("ALUMNO:"+alumno.getNombre()+" "+alumno.getApellidos());
        table.addCell("CICLO FORMATIVO: "+ciclo.getNombre());
        return table;
    }

    /**
     * Tabla del seguimiento de practicas de lunes a viernes vacia
     * @return PdfPTable
     */
    public static PdfPTable seguimientoPracticas(){

        // tabla con dos columnas
        PdfPTable table = new PdfPTable(4);
        try {
            table.setWidthPercentage(100);
            table.setWidths(new int[] {1,4,2,2});
        } catch (DocumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        table.setHorizontalAlignment(Element.ALIGN_LEFT);

        PdfPCell cell;
        cell = new PdfPCell(new Phrase("DÍAS"));
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("ACTIVIDADES DESARROLLADAS"));
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("TIEMPO EMPLEADO"));
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("OBSERVACIONES"));
        table.addCell(cell);

        for(int i=0;i<5;i++) {
            switch(i) {

                case 0:
                    cell = new PdfPCell(new Phrase("LUNES"));
                    table.addCell(cell);
                    break;

                case 1:
                    cell = new PdfPCell(new Phrase("MARTES"));
                    table.addCell(cell);
                    break;

                case 2:
                    cell = new PdfPCell(new Phrase("MIÉRCOLES"));
                    table.addCell(cell);
                    break;

                case 3:
                    cell = new PdfPCell(new Phrase("JUEVES"));
                    table.addCell(cell);
                    break;

                case 4:
                    cell = new PdfPCell(new Phrase("VIERNES"));
                    table.addCell(cell);
                    break;
            }

            cell = new PdfPCell(new Phrase("\n\n\n"));
            table.addCell(cell);

            cell = new PdfPCell();
            table.addCell(cell);

            cell = new PdfPCell();
            table.addCell(cell);
        }

        return table;
    }

    /**
     * Tabla del seguimiento de practicas de lunes a viernes rellena
     * @param actividades
     * @return PdfPTable
     */
    public static PdfPTable seguimientoPracticas(ArrayList<Actividades> actividades){

        // tabla con dos columnas
        PdfPTable table = new PdfPTable(4);
        try {
            table.setWidthPercentage(100);
            table.setWidths(new int[] {1,4,2,2});
        } catch (DocumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        table.setHorizontalAlignment(Element.ALIGN_LEFT);

        PdfPCell cell;
        cell = new PdfPCell(new Phrase("DÍAS"));
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("ACTIVIDADES DESARROLLADAS"));
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("TIEMPO EMPLEADO"));
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("OBSERVACIONES"));
        table.addCell(cell);

        for (Actividades actividad:actividades) {
            cell = new PdfPCell(new Phrase(actividad.getDia()));
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(actividad.getActividades()));
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(actividad.getTiempoEmpleado()));
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(actividad.getObservaciones()));
            table.addCell(cell);
        }

        return table;
    }

    /**
     * Hace en el PDF el parrafo que corresponde a la firma del alumno
     * @return Paragraph
     */
    public static Paragraph firmas() {
        Paragraph firmas = new Paragraph();

        firmas.add("FIRMA DEL ALUMNO                                EL RESPONSABLE DE LA EMPRESA                                EL PROFESOR-TUTOR DEL CENTRO\n\n\n");

        firmas.add("Fdo:                                                            Fdo:                                                                                        Fdo:");

        return firmas;
    }
}
