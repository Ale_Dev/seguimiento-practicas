package alex.proyectos.proyectoFinal.controllers;

import alex.proyectos.proyectoFinal.modelos.Responsable;
import alex.proyectos.proyectoFinal.repositorys.ResponsableRepository;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.Optional;

/**
 * @author alex
 * Realiza las consultas a la BBDD relacionadas con los Responsables que seran necesarias
 */

@Controller
@RequestMapping(path = "/responsables")//URL principal
public class ResponsableController {

    //interfaz repositorio que hace tiene los metodos del CRUD
    @Autowired
    private ResponsableRepository repository;

    /**
     * Recoge todos los Responsables de la BBDD
     * @return Iterable<Responsable>
     */
    @GetMapping(path = "/all")//URL a la que se accede para hacer la consulta a la BBDD
    @CrossOrigin(origins = "http://localhost:4200")//permite a Angular conectar a la REST API
    public @ResponseBody Iterable<Responsable> getResponsables(){
        return repository.findAll();
    }

    /**
     * Recoge de la BBDD un Responsable con un ID determinado
     * @param id
     * @return Optional<Responsable>
     */
    @GetMapping(path = "/{id}")
    @CrossOrigin(origins = "http://localhost:4200")
    public @ResponseBody
    Optional<Responsable> getResponsable(@PathVariable("id") Integer id){
        return repository.findById(id);
    }

    /**
     * Busca los Responsables segun la Empresa a la que pertenezcan
     * @param idEmpresa
     * @return Iterable<Responsable>
     */
    @GetMapping(path = "/empresa/{id}")
    @CrossOrigin(origins = "http://localhost:4200")
    public @ResponseBody Iterable<Responsable> getResponsablesEmpresa(@PathVariable("id") Integer idEmpresa){
        return repository.findByIdEmpresaAux(idEmpresa);
    }

    /**
     * Crea un Responsable en la BBDD
     * @param responsable
     * @return Responsable
     */
    @PostMapping(path = "/add")
    @CrossOrigin(origins = "http://localhost:4200")
    public @ResponseBody Responsable crearResponsable(@RequestBody Responsable responsable){
        return repository.save(responsable);
    }

    /**
     * Borra un Responsable con un ID determinado
     * @param id
     */
    @DeleteMapping(path = {"/delete/{id}"})
    @CrossOrigin(origins = "http://localhost:4200")
    @Transactional
    @OnDelete(action= OnDeleteAction.CASCADE)
    public @ResponseBody
    void borrarResponsable(@PathVariable("id")Integer id){
        repository.deleteById(id);
    }

}
