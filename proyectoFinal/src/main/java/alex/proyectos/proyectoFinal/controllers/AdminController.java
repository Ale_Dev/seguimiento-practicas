package alex.proyectos.proyectoFinal.controllers;

import alex.proyectos.proyectoFinal.modelos.Admin;
import alex.proyectos.proyectoFinal.modelos.Ciclo;
import alex.proyectos.proyectoFinal.repositorys.AdminRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/admin")//URL principal
public class AdminController {

    @Autowired
    private AdminRepository repository;

    /**
     * Consulta todos los Administradores de la BBDD
     * @return Iterable<Admin>
     */
    @GetMapping(path = "/all")//URL a la que se accede para hacer la consulta a la BBDD
    @CrossOrigin(origins = "http://localhost:4200")//permite a Angular conectar a la REST API
    public @ResponseBody
    Iterable<Admin>getAdmins(){
        return repository.findAll();
    }
}
