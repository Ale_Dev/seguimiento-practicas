package alex.proyectos.proyectoFinal.modelos;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;

@Entity
@Table(name = "actividades")//Indicamos al ORM que tabla de la BBDD es
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Actividades {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Cascade(org.hibernate.annotations.CascadeType.DELETE)
    private Integer idActividades;

    @Column
    private String dia;

    @Column
    private String actividades;

    @Column
    private String tiempoEmpleado;

    @Column
    private String observaciones;

    @Column
    private Integer idSeguimientoAux;

    public Integer getIdActividades() {
        return idActividades;
    }

    public void setIdActividades(Integer idActividades) {
        this.idActividades = idActividades;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getActividades() {
        return actividades;
    }

    public void setActividades(String actividades) {
        this.actividades = actividades;
    }

    public String getTiempoEmpleado() {
        return tiempoEmpleado;
    }

    public void setTiempoEmpleado(String tiempoEmpleado) {
        this.tiempoEmpleado = tiempoEmpleado;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Integer getIdSeguimientoAux() {
        return idSeguimientoAux;
    }

    public void setIdSeguimientoAux(Integer idSeguimientoAux) {
        this.idSeguimientoAux = idSeguimientoAux;
    }
}
