package alex.proyectos.proyectoFinal.modelos;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;

@Entity
@Table(name = "responsable")//Indicamos al ORM que tabla de la BBDD es
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Responsable
{
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Cascade(org.hibernate.annotations.CascadeType.DELETE)
    private Integer idResponsable;

    @Column
    private String nombre;

    @Column
    private String apellidos;

    @Column
    private Integer idEmpresaAux;

    public Integer getIdResponsable() {
        return idResponsable;
    }

    public void setIdResponsable(Integer idResponsable) {
        this.idResponsable = idResponsable;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public Integer getIdEmpresaAux() {
        return idEmpresaAux;
    }

    public void setIdEmpresaAux(Integer idEmpresaAux) {
        this.idEmpresaAux = idEmpresaAux;
    }
}
