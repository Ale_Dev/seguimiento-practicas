package alex.proyectos.proyectoFinal.modelos;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;

@Entity
@Table(name = "empresa")//Indicamos al ORM que tabla de la BBDD es
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Empresa {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Cascade(org.hibernate.annotations.CascadeType.DELETE)
    private Integer idEmpresa;

    @Column
    private String nombre;

    @Column
    private String direccion;

    @Column
    private Integer telefono;

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion1() {
        return direccion;
    }

    public void setDireccion1(String direccion1) {
        this.direccion = direccion1;
    }

    public Integer getTelefono1() {
        return telefono;
    }

    public void setTelefono1(Integer telefono1) {
        this.telefono = telefono1;
    }

}
