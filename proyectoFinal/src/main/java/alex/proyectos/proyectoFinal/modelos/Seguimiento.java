package alex.proyectos.proyectoFinal.modelos;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "seguimiento")//Indicamos al ORM que tabla de la BBDD es
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Seguimiento {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idSeguimiento;

    @Column
    private String diaInicio;

    @Column
    private String diaFinal;

    @Column
    private String mes;

    @Column
    private Integer anio;

    @Column
    private Integer idAlumnoAux;

    public Integer getIdSeguimiento() {
        return idSeguimiento;
    }

    public void setIdSeguimiento(Integer idSeguimiento) {
        this.idSeguimiento = idSeguimiento;
    }

    public String getDiaInicio() {
        return diaInicio;
    }

    public void setDiaInicio(String diaInicio) {
        this.diaInicio = diaInicio;
    }

    public String getDiaFinal() {
        return diaFinal;
    }

    public void setDiaFinal(String diaFinal) {
        this.diaFinal = diaFinal;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    public Integer getIdAlumnoAux() {
        return idAlumnoAux;
    }

    public void setIdAlumnoAux(Integer idAlumnoAux) {
        this.idAlumnoAux = idAlumnoAux;
    }
}
