import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { HttpClientModule } from '@angular/common/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SeguimientosPage } from '../pages/seguimientos/seguimientos';
import { ActividadesPage } from '../pages/actividades/actividades';
import { AlumnoProvider } from '../providers/alumno/alumno';
import { SeguimientoProvider } from '../providers/seguimiento/seguimiento';
import { ActividadesProvider } from '../providers/actividades/actividades';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SeguimientosPage,
    ActividadesPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SeguimientosPage,
    ActividadesPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AlumnoProvider,
    SeguimientoProvider,
    ActividadesProvider
  ]
})
export class AppModule {}
