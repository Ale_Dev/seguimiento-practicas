export class Actividades {

    idActividades: string;
    dia: string;
    actividades: string;
    tiempoEmpleado: string;
    observaciones: string;
    idSeguimientoAux: string;
}