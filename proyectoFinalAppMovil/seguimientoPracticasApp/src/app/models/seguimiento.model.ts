export class Seguimiento {
    idSeguimiento: string;
    diaInicio: string;
    diaFinal: string;
    mes: string;
    anio: string;
    idAlumnoAux;
}