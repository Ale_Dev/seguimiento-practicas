import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { Alumno } from '../../app/models/alumno.model';

/*
  Generated class for the AlumnoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AlumnoProvider {

  public API = '//localhost:8080';
  public ALUMNO_API = this.API + '/alumnos';

  constructor(public http: HttpClient) {
    console.log('Hello AlumnoProvider Provider');
  }

  getAll(): Observable<any> {
    console.log("ALUMNO PROVIDER GET ALL");
    return this.http.get<Alumno[]>(this.ALUMNO_API + '/all');
  }

}
