import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Actividades } from '../../app/models/actividades.model';

/*
  Generated class for the SeguimientoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ActividadesProvider {

  public API = '//localhost:8080';
  public ACTIVIDADES_API = this.API + '/actividades';

  constructor(public http: HttpClient) {
    console.log('Hello ActividadesProvider Provider');
  }

  createActividad(actividad: Actividades){
    return this.http.post<Actividades>(this.ACTIVIDADES_API + '/add', actividad);
  }

  getAllByIdSeguimiento(idSeguimiento: string): Observable<any>{
    return this.http.get(this.ACTIVIDADES_API + '/' + idSeguimiento);
  }

  public deleteActividades(idSeguimiento: string) {
    return this.http.delete(this.ACTIVIDADES_API + "/delete/"+ idSeguimiento);
  }

  public pdfFull(id){
    return this.http.get(this.API + "/pdf/"+id, { responseType: 'blob', 
    headers: new HttpHeaders().append('ContentType','application/json')
    });
  }

}
