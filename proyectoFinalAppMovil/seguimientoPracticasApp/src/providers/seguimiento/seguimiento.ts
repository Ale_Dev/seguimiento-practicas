import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Seguimiento } from '../../app/models/seguimiento.model';

/*
  Generated class for the SeguimientoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SeguimientoProvider {

  public API = '//localhost:8080';
  public SEGUIMIENTO_API = this.API + '/seguimientos';

  constructor(public http: HttpClient) {
    console.log('Hello SeguimientoProvider Provider');
  }

  getAll(): Observable<any> {
    return this.http.get<Seguimiento[]>(this.SEGUIMIENTO_API + '/all');
  }

  getByAlumno(id: string){
    return this.http.get<Seguimiento[]>(this.SEGUIMIENTO_API + '/alumno/' + id);
  }

  createSeguimiento(seguimiento){
    return this.http.post<Seguimiento>(this.SEGUIMIENTO_API + '/add', seguimiento);
  }

  public deleteSeguimiento(seguimiento: Seguimiento){
    return this.http.delete(this.SEGUIMIENTO_API + '/delete/' + seguimiento.idSeguimiento);
  }

  public pdfNull(){
    return this.http.get(this.API + "/pdf/null", { responseType: 'blob', 
    headers: new HttpHeaders().append('ContentType','application/json')
    });
  }

}
