import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ToastController } from 'ionic-angular';

import { Actividades } from '../../app/models/actividades.model';
import { ActividadesProvider } from '../../providers/actividades/actividades';

/**
 * Generated class for the ActividadesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-actividades',
  templateUrl: 'actividades.html',
})
export class ActividadesPage {

  listaActividades: Actividades[];

  lunes: Actividades = {
    "idActividades": "",
    "dia": "Lunes",
    "actividades": "",
    "tiempoEmpleado": "",
    "observaciones": "",
    "idSeguimientoAux": ""
  }

  martes: Actividades = {
    "idActividades": "",
    "dia": "Martes",
    "actividades": "",
    "tiempoEmpleado": "",
    "observaciones": "",
    "idSeguimientoAux": ""
  }

  miercoles: Actividades = {
    "idActividades": "",
    "dia": "Miercoles",
    "actividades": "",
    "tiempoEmpleado": "",
    "observaciones": "",
    "idSeguimientoAux": ""
  }

  jueves: Actividades = {
    "idActividades": "",
    "dia": "Jueves",
    "actividades": "",
    "tiempoEmpleado": "",
    "observaciones": "",
    "idSeguimientoAux": ""
  }

  viernes: Actividades = {
    "idActividades": "",
    "dia": "Viernes",
    "actividades": "",
    "tiempoEmpleado": "",
    "observaciones": "",
    "idSeguimientoAux": ""
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private actividadesProvider: ActividadesProvider, public toastCtrl: ToastController) {
  }

  ngOnInit() {
    this.actividadesProvider.getAllByIdSeguimiento(this.navParams.get("idSeguimiento")).subscribe((actividades: any) => {
      if(actividades){
        console.log("AQUI");
        this.listaActividades = actividades;
      
        for(let i in this.listaActividades){
          switch(i){
            case "0":
              this.lunes = this.listaActividades[i];
              break;
            case "1":
              this.martes = this.listaActividades[i];
              break;
            case "2":
              this.miercoles = this.listaActividades[i];
              break;
            case "3":
              this.jueves = this.listaActividades[i];
              break;
            case "4":
              this.viernes = this.listaActividades[i];
              break;
          }
        }

        this.lunes.idSeguimientoAux = this.navParams.get("idSeguimiento");
        this.martes.idSeguimientoAux = this.navParams.get("idSeguimiento");
        this.miercoles.idSeguimientoAux = this.navParams.get("idSeguimiento");
        this.jueves.idSeguimientoAux = this.navParams.get("idSeguimiento");
        this.viernes.idSeguimientoAux = this.navParams.get("idSeguimiento");
      }
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ActividadesPage');
  }

  guardarActividades(){
    this.actividadesProvider.createActividad(this.lunes).subscribe(result => {
      this.actividadesProvider.createActividad(this.martes).subscribe(result => {
        this.actividadesProvider.createActividad(this.miercoles).subscribe(result => {
          this.actividadesProvider.createActividad(this.jueves).subscribe(result => {
            this.actividadesProvider.createActividad(this.viernes).subscribe(result => {
              this.toastSuccess();
            });
          });
        });
      });
    });
  }

  toastSuccess() {
    const toast = this.toastCtrl.create({
      message: 'Actividades Guardadas',
      duration: 3000
    });
    toast.present();
  }
}
