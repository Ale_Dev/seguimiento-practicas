import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ToastController } from 'ionic-angular';

import { HomePage } from '../../pages/home/home';

import { Seguimiento } from '../../app/models/seguimiento.model';
import { SeguimientoProvider } from '../../providers/seguimiento/seguimiento';

import { ActividadesPage } from '../../pages/actividades/actividades';
import { ActividadesProvider } from '../../providers/actividades/actividades';

/**
 * Generated class for the SeguimientosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-seguimientos',
  templateUrl: 'seguimientos.html',
})
export class SeguimientosPage {

  seguimiento: Seguimiento = {
    "idSeguimiento": "",
    "diaInicio": "",
    "diaFinal": "",
    "mes": "",
    "anio": "",
    "idAlumnoAux": ""
  }

  //seguimiento: Seguimiento = new Seguimiento();

  seguimientos: Seguimiento[] = new Array<Seguimiento>();

  nuevoSeguimiento: any = {
    "inicio": "",
    "final": ""
  };

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public toastCtrl: ToastController, private seguimientoProvider: SeguimientoProvider,
    private actividadesProvider: ActividadesProvider) {

      this.seguimiento.idAlumnoAux = this.navParams.get("idAlumno");
  }

  ngOnInit() {
    this.getSeguimientos(this.navParams.get("idAlumno"));
  }

  getSeguimientos(id: string){
    this.seguimientoProvider.getByAlumno(id).subscribe(seg => {
      this.seguimientos = seg;
    });
  }

  addSeguimiento(){
    let yearActual = (new Date()).getFullYear();

    let inicioSplit: number[];
    let finalSplit: number[];

    if(this.nuevoSeguimiento.inicio == "" || this.nuevoSeguimiento.final == ""){
      this.toastError();
    }else{

      /**
       * Split[0]: Año
       * Split[1]: Mes
       * Split[2]: Dia
       */
      inicioSplit = this.nuevoSeguimiento.inicio.split("-");
      finalSplit = this.nuevoSeguimiento.final.split("-");

      if(inicioSplit[0]!=yearActual || finalSplit[0]!=yearActual || finalSplit[1]<inicioSplit[1]){
        this.toastError();
      }else{
        if(inicioSplit[1] == finalSplit[1] || inicioSplit[1] < finalSplit[1]){
          if(inicioSplit[1] < finalSplit[1]){
            this.generarSeguimiento(inicioSplit, finalSplit);
          }else{
            if(inicioSplit[2]>finalSplit[2]){
              this.toastError();
            }else{
              this.generarSeguimiento(inicioSplit, finalSplit);
            }
          }
        }
      }
    }
  }

  generarSeguimiento(inicioSplit, finalSplit){
    this.seguimiento.diaInicio = inicioSplit[2].toString();
            this.seguimiento.diaFinal = finalSplit[2].toString();

            switch(finalSplit[1].toString()){
              case "01":
                this.seguimiento.mes = "Enero";
                break;
              
              case "02":
                this.seguimiento.mes = "Febrero";
                break;

              case "03":
                this.seguimiento.mes = "Marzo";
                break;

              case "04":
                this.seguimiento.mes = "Abril";
                break;

              case "05":
                this.seguimiento.mes = "Mayo";
                break;

              case "06":
                this.seguimiento.mes = "Junio";
                break;

              case "07":
                this.seguimiento.mes = "Julio";
                break;

              case "08":
                this.seguimiento.mes = "Agosto";
                break;

              case "09":
                this.seguimiento.mes = "Septiembre";
                break;

              case "10":
                this.seguimiento.mes = "Octubre";
                break;

              case "11":
                this.seguimiento.mes = "Noviembre";
                break;

              case "12":
                this.seguimiento.mes = "Diciembre";
                break;
            }

            this.seguimiento.anio = finalSplit[0].toString();

            this.seguimiento.idAlumnoAux = this.navParams.get("idAlumno");

            console.log(this.seguimiento.diaInicio + " " + this.seguimiento.diaFinal + " " + 
              this.seguimiento.mes + " " + this.seguimiento.anio + " " + 
              this.seguimiento.idAlumnoAux);

              this.seguimientoProvider.createSeguimiento(this.seguimiento).subscribe(result => {
                this.seguimientoProvider.getAll().subscribe(segs => {
                  this.seguimientos = segs;
                });
              });
  }

  editarSeguimiento(seguimiento: Seguimiento){
    this.navCtrl.push(ActividadesPage, {
      idSeguimiento: seguimiento.idSeguimiento
    });
  }

  borrarSeguimiento(seguimiento: Seguimiento){

    this.actividadesProvider.deleteActividades(seguimiento.idSeguimiento).subscribe(result => {
      this.seguimientoProvider.deleteSeguimiento(seguimiento).subscribe(data => {
        this.seguimientos = this.seguimientos.filter(e => e!== seguimiento);
      });
    });
  }

  toastError() {
    const toast = this.toastCtrl.create({
      message: 'Fecha Incorrecta',
      duration: 3000
    });
    toast.present();
  }

  logOut(){
    this.navCtrl.push(HomePage);
  }

}
