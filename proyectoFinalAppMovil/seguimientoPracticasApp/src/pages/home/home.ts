import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';//navegar entre paginas
import { ToastController } from 'ionic-angular';//para los errores

import { Alumno } from '../../app/models/alumno.model';//clase Alumno
import { AlumnoProvider } from '../../providers/alumno/alumno';//servicios HTTP de Alumno

import { SeguimientosPage } from '../../pages/seguimientos/seguimientos';//Pagina a la que se navega

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  alumnos: Alumno[] = new Array<Alumno>();

  //Objeto que se rellena en el Log In, primero solo E-Mail y Pass, al validar, se rellena entero
  alumno: Alumno = {
    "idAlumno": " ",
    "nombre": " ",
    "apellidos": " ",
    "email": "",
    "pass": "",
    "idResponsableAux": " ",
    "idCicloAux": " ",
  }

  constructor(public navCtrl: NavController, 
  private alumnoProvider: AlumnoProvider,
  public toastCtrl: ToastController) {

  }

  ngOnInit(){
    console.log("EMPIEZA ON INIT");
    //se sacan todos los alumnos de la Base de Datos al abrir la App
    this.alumnoProvider.getAll().subscribe(alumnos => {
      console.log("ANTES DEL SUBSCRIBE");
      this.alumnos = alumnos;
      console.log("DESPUES DEL SUBSCRIBE");
      console.log(this.alumnos.length);
    });
  }

  /**
   * Se comprueba que el usuario ha rellenado todos los campos
   * Se validan E-Mail y Pass
   */
  logear(){
    let sesionIniciada = false;

    if(this.alumno.email == "" || this.alumno.pass == ""){
      this.toastError();
    }else{
      for(let i in this.alumnos){
        let alumnoActual = this.alumnos[i];

        if(this.alumno.email == alumnoActual.email &&
           this.alumno.pass == alumnoActual.pass){
             this.alumno = alumnoActual;
             sesionIniciada = true;
           }
      }

      /**
       * Si la sesion se ha iniciado se pasa a la siguiente pagina junto con el idAlumno
       * Sino, salta error
       */
      if(sesionIniciada){
        this.navCtrl.push(SeguimientosPage, {
          idAlumno: this.alumno.idAlumno
        });
      }else{
        this.toastError();
      }
  }
}

//notificacion de que la validacion ha fallado (datos incompletos/datos incorrectos)
toastError() {
  const toast = this.toastCtrl.create({
    message: 'E-Mail o Contraseña incorrectos',
    duration: 3000
  });
  toast.present();
}

}
